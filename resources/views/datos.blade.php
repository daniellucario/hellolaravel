<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Contacto</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/shards-ui/3.0.0/css/shards.css">
</head>

<body>
  <div class="container d-flex justify-content-center align-items-center">
    <div class="col-3 mt-5">
      <div class="">
        <h4>Enviar mensaje</h4>
        <form method="post" action="{{ route('datos.store') }}">

        @if (count($errors) > 0)
        <div class="text-danger border-danger border p-3">
            @foreach ($errors->all() as $error)
            <div>{{ $error }} </div>
            @endforeach
        </div>
        @endif
        
          <div class="form-group mt-3">
            <input type="text" name="nombre" class="form-control" placeholder="Escribe tu nombre" value="{{old('nombre')}}">
          </div>

          <div class="form-group mt-3">
            <input type="email" name="email" class="form-control"placeholder="Escribe tu email" value="{{old('email')}}">
          </div>

          <div class="form-group mt-3">
            <textarea name="mensaje" class="form-control"placeholder="Escribe tus comentarios" value="{{old('mensaje')}}"></textarea>
          </div>

          <div class="form-group mt-3">
            <input type="submit" value="Enviar" class="btn btn-primary w-100 btn-pill">
          </div>
          {{ csrf_field() }}
        </form>
      </div>
    </div>
  </div>
</body>
