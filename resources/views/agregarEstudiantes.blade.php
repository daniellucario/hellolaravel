<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Agregar estudiantes</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/shards-ui/3.0.0/css/shards.css">
</head>
<body>
    <div clas="w-100">
        <div class="container d-flex justify-content-center align-items-center">
            <div class="col-3 mt-5">
                <h4>Agregar estudiantes</h4>
                <form method="post" action="{{route ('estudiantes.store')}}">
                    <div class="form-group">
                        <input type="text" name="Matricula" class="form-control" placeholder="Matricula" value="{{old('Matricula')}}">
                    </div>
                    <div class="form-group">
                        <input type="text" name="Nombre" class="form-control" placeholder="Nombre" value="{{old('Nombre')}}">
                    </div>
                    <div class="form-group">
                        <input type="text" name="Direccion" class="form-control" placeholder="Dirección" value="{{old('Direccion')}}"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Enviar" class="btn btn-primary w-100 btn-pill">
                        {{ csrf_field() }}
                    </div>
                    @if (count($errors) > 0)
                    <div class="text-danger border-danger border p-3">
                        @foreach ($errors->all() as $error)
                        <div>{{ $error }} </div>
                        @endforeach
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
</body>

</html