<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lista de estudiantes</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/shards-ui/3.0.0/css/shards.css">
</head>

<body>
    <div class="container pt-5">
        <h4 class="font-weight-normal">Lista de <b>estudiantes</b></h4>
        <hr>
        <form action="{{route('lista.index')}}" method="GET" class="row mb-3 mt-3">
            <div class="col-sm-4 row m-auto">
                <input type="text" class="form-control col" name="texto" value="{{$texto}}">
            </div>
            <div class="col">
                <input type="submit" class="btn btn-success text-uppercase" value="buscar">
            </div>
            <div class="col d-flex justify-content-end">
                <a href="/estudiantes" class="btn btn-primary text-uppercase mx-2">
                    añadir
                </a>
                <a class="btn btn-primary text-uppercase" href="{{route('descargaPDF')}}">DESCARGAR PDF</a>
            </div>
        </form>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th> Matrícula</th>
                        <th> Nombre</th>
                        <th> Dirección </th>
                        <th> Acciones </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($estudiante as $estudiantes)
                    <tr>
                        <td>{{$estudiantes->Matricula}}</td>
                        <td>{{$estudiantes->Nombre}}</td>
                        <td>{{$estudiantes->Direccion}}</td>
                        <td class="col-2">
                            <form class="d-flex" method="POST" action="{{ url(" lista/{$estudiantes->Matricula}")
                                }}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-outline-danger btn-small btn-squared btn-sm" type="submit"
                                    onclick="return confirm ('¿Deseas Borrar?');">Eliminar</button>
                                <a href="{{ route ('Lista.edit',$estudiantes->id) }}"
                                    class="btn btn-outline-secondary btn-small btn-squared btn-sm">Actualizar</a>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>