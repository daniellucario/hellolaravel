<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/shards-ui/3.0.0/css/shards.css">
</head>

<body>
    <table class="table table-striped">
        <thead class="thead-light">
            <tr>
                <th> Matrícula</th>
                <th> Nombre</th>
                <th> Dirección </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($estudiante as $estudiantes)
            <tr>
                <td>{{$estudiantes->Matricula}}</td>
                <td>{{$estudiantes->Nombre}}</td>
                <td>{{$estudiantes->Direccion}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>