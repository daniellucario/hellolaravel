<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\estudiantes;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\validaEdicion;

class listaController extends Controller
{
    //
    public function index(Request $request){
        $texto=trim($request->get('texto'));
        if($texto==''){
            $estudiante = estudiantes::all();
        }else{
            $estudiante= DB::table('estudiantes')->select('Matricula','Nombre','Direccion')
                ->where('Matricula','=',$texto)->paginate(10);
        }
        return view ('vistaEstudiante', compact('estudiante','texto'));
    }

    public function getAll(){
        $estudiante = estudiantes::all();
        return view ('grafica', compact('estudiante'));
    }

    public function destroy($matricula){
        $estudiante= DB::table('estudiantes')->select('Matricula')
                ->where('Matricula','=',$matricula);
        $estudiante->delete();
        return redirect()->route('lista.index');
    }
    /*
    public function edit($matricula)
    {
        $estudiante= DB::table('estudiantes')
                ->where('Matricula',$matricula)->first();
        return view('EditaEstudiante', compact('estudiante'));
    }
    */

    public function edit($id)
    {
        $estudiante = estudiantes::whereId($id)->firstOrFail();
        return view('EditaEstudiante', compact('estudiante'));
    }
    /*
    public function update(validaEdicion $request)
    {
        $Nombre =$request->input('Nombre');
        $Matricula = $request->input('Matricula');
        $Direccion =$request->input('Direccion');

        $estudiante= DB::table('estudiantes')
                ->where('Matricula',$Matricula)
                ->update(['Matricula'=>$Matricula,'Nombre'=>$Nombre,'Direccion'=>$Direccion]);
  
        return redirect()->route('lista.index');
    }
*/
    public function update(validaEdicion $request, $id)
    {
        $estudiante= estudiantes::findOrFail($id);
        $estudiante->Matricula = $request->input('Matricula');
        $estudiante->Nombre =$request->input('Nombre');
        $estudiante->Direccion =$request->input('Direccion');
        $estudiante->save();
        return redirect()->route('lista.index');
    }
}
 