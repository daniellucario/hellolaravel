<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\estudiantes;
use App\Http\Requests\estudiantesRequest;

class controllerEstudiantes extends Controller
{
    public function mostrar(){
        return view ('AgregarEstudiantes');
    }

    public function store(estudiantesRequest $request){
        $estudiantes=new estudiantes();
        $estudiantes->Matricula=$request->Matricula;
        $estudiantes->Nombre=$request->Nombre;
        $estudiantes->Direccion=$request->Direccion;
        $estudiantes-> save();
        return redirect('estudiantes');
    }

}
