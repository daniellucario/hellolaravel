<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\validaRequest;
use App\Http\Controllers\Controller;

class datosController extends Controller
{
    public function mostrar(){
        return view ('datos');
    }
    public function store(validaRequest $request){
        return $request->all();
    }
}
 